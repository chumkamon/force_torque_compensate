# generated from genmsg/cmake/pkg-genmsg.context.in

messages_str = "/home/birl/iiwa-stack/src/iiwa_msgs/msg/CartesianControlModeLimits.msg;/home/birl/iiwa-stack/src/iiwa_msgs/msg/CartesianEulerPose.msg;/home/birl/iiwa-stack/src/iiwa_msgs/msg/CartesianImpedanceControlMode.msg;/home/birl/iiwa-stack/src/iiwa_msgs/msg/CartesianPlane.msg;/home/birl/iiwa-stack/src/iiwa_msgs/msg/CartesianQuantity.msg;/home/birl/iiwa-stack/src/iiwa_msgs/msg/CartesianVelocity.msg;/home/birl/iiwa-stack/src/iiwa_msgs/msg/ControlMode.msg;/home/birl/iiwa-stack/src/iiwa_msgs/msg/DOF.msg;/home/birl/iiwa-stack/src/iiwa_msgs/msg/DesiredForceControlMode.msg;/home/birl/iiwa-stack/src/iiwa_msgs/msg/JointDamping.msg;/home/birl/iiwa-stack/src/iiwa_msgs/msg/JointImpedanceControlMode.msg;/home/birl/iiwa-stack/src/iiwa_msgs/msg/JointPosition.msg;/home/birl/iiwa-stack/src/iiwa_msgs/msg/JointPositionVelocity.msg;/home/birl/iiwa-stack/src/iiwa_msgs/msg/JointQuantity.msg;/home/birl/iiwa-stack/src/iiwa_msgs/msg/JointStiffness.msg;/home/birl/iiwa-stack/src/iiwa_msgs/msg/JointTorque.msg;/home/birl/iiwa-stack/src/iiwa_msgs/msg/JointVelocity.msg;/home/birl/iiwa-stack/src/iiwa_msgs/msg/SinePatternControlMode.msg"
services_str = "/home/birl/iiwa-stack/src/iiwa_msgs/srv/ConfigureSmartServo.srv;/home/birl/iiwa-stack/src/iiwa_msgs/srv/SetPathParameters.srv;/home/birl/iiwa-stack/src/iiwa_msgs/srv/TimeToDestination.srv"
pkg_name = "iiwa_msgs"
dependencies_str = "std_msgs;geometry_msgs"
langs = "gencpp;geneus;genlisp;gennodejs;genpy"
dep_include_paths_str = "iiwa_msgs;/home/birl/iiwa-stack/src/iiwa_msgs/msg;std_msgs;/opt/ros/kinetic/share/std_msgs/cmake/../msg;geometry_msgs;/opt/ros/kinetic/share/geometry_msgs/cmake/../msg"
PYTHON_EXECUTABLE = "/usr/bin/python"
package_has_static_sources = '' == 'TRUE'
genmsg_check_deps_script = "/opt/ros/kinetic/share/genmsg/cmake/../../../lib/genmsg/genmsg_check_deps.py"
